import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "instagram",
  url: "https://www.instagram.com/alexanderkachkaev",
});
