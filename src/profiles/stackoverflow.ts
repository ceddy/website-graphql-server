import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "stackoverflow",
  url: "https://meta.stackoverflow.com/users/1818285/alexander-kachkaev",
});
