import executeTwitterApiFetchTask from "../lib/fetchTaskExecutors/twitterApi";
import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "twitter-en",
  url: "https://twitter.com/kachkaev",
  fetchTasks: [
    (context) =>
      executeTwitterApiFetchTask(context, {
        userName: "kachkaev",
      }),
  ],
});
