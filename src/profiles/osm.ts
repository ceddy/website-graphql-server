import executeXmlFetchTask from "../lib/fetchTaskExecutors/xml";
import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "osm",
  url: "https://www.openstreetmap.org/user/Kachkaev",
  fetchTasks: [
    (context) =>
      executeXmlFetchTask(context, {
        // username -> id mapping via http://whosthat.osmz.ru/
        url: "https://www.openstreetmap.org/api/0.6/user/231451",
        collectData: (data) => {
          const changesetCount = data.osm.user[0].changesets[0].$.count;
          const gpsTraceCount = data.osm.user[0].traces[0].$.count;
          if (!changesetCount || !gpsTraceCount) {
            throw new Error(
              `Expected positive values for changesetCount and gpsTraceCount, ${changesetCount} and ${gpsTraceCount} given`,
            );
          }
          return {
            changesetCount: changesetCount * 1,
            gpsTraceCount: gpsTraceCount * 1,
          };
        },
      }),
  ],
});
