import executeJsonFetchTask from "../lib/fetchTaskExecutors/json";
import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "gitlab",
  url: "https://gitlab.com/kachkaev",
  fetchTasks: [
    (context) =>
      executeJsonFetchTask(context, {
        url: "https://gitlab.com/api/v4/users/741429/projects",
        collectData: (json) => {
          return {
            repoCount: json.length,
          };
        },
      }),
  ],
});
