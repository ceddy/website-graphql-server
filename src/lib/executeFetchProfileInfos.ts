import { createApolloFetch } from "apollo-fetch";
import * as _ from "lodash";
import { env } from "../config";

const query = /* GraphQL */ `
mutation FetchProfileInfos($securityToken: String!) {
  fetchProfileInfos(input: { securityToken: $securityToken }) {
    results {
      profile {
        name
      }
      success
    }
  }
}`;

export default async ({ uri, when, terminate }) => {
  try {
    console.log(`Fetching profiles ${when}...`);

    const apolloFetch = createApolloFetch({ uri });
    const mutationResult = await apolloFetch({
      query,
      variables: {
        securityToken: env.FETCH_SECURITY_TOKEN,
      },
    });

    if (mutationResult.errors) {
      throw new Error(mutationResult.errors[0].toString());
    }
    const profileNamesThatFailed = _.map(
      _.filter(
        _.get(mutationResult, ["data", "fetchProfileInfos", "results"]),
        ["success", false],
      ),
      (r) => _.get(r, ["profile", "name"]),
    );
    console.log(
      `Fetched profiles ${when}. Unsuccessful: ${
        profileNamesThatFailed.length
          ? profileNamesThatFailed.join(", ")
          : "none"
      }`,
    );
  } catch (e) {
    console.log(`Failed fetching profiles ${when}.`);
    terminate(e);
  }
};
