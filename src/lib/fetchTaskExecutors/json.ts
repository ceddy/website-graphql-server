import { writeFile } from "fs-extra";
import fetch from "node-fetch";
import { FetchTaskContext } from "../generateProfile";

export default async (
  { errorReportPathWithoutExtension }: FetchTaskContext,
  { url, collectData },
) => {
  try {
    const json = await (await fetch(url)).json();
    return collectData(json);
  } catch (e) {
    await writeFile(`${errorReportPathWithoutExtension}.txt`, e);
    throw new Error();
  }
};
