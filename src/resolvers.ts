import * as GraphQLJson from "graphql-type-json";
import * as GraphQLRegExp from "graphql-type-regexp";
import * as _ from "lodash";
import { env } from "./config";
import { acquirePin } from "./profiles/linkedin";

// tslint:disable-next-line:no-var-requires
const allImportedProfiles = require("require-dir")(`${__dirname}/profiles`);
const profiles = _.map(_.values(allImportedProfiles), "default");
const profilesByName = _.keyBy(profiles, "name");

const verifySecurityToken = (securityToken) => {
  if (securityToken !== env.FETCH_SECURITY_TOKEN) {
    throw new Error("Provided securityToken does not match the expected one");
  }
};

export default {
  JSON: GraphQLJson,

  RegExp: GraphQLRegExp,

  Profile: {
    info: ({ name, info }) => info || profilesByName[name].getInfo(),
    infoFetchedAt: ({ name }) => profilesByName[name].getInfoFetchedAt(),
  },

  Query: {
    unixTimestamp() {
      return Math.round(+new Date() / 1000);
    },

    async profiles(obj, { filter = /.*/, onlyWithInfo = false }) {
      let matchingProfiles = _.filter(profiles, ({ name }) =>
        filter.test(name),
      );
      if (onlyWithInfo) {
        // https://stackoverflow.com/questions/46619596/apollo-graphql-server-filter-or-sort-by-a-filed-that-is-resolved-separately
        const promises = _.map(matchingProfiles, ({ name }) =>
          profilesByName[name].getInfo(),
        );
        const infos = await Promise.all(promises);
        for (let i = 0; i < infos.length; i += 1) {
          matchingProfiles[i].info = infos[i];
        }
        matchingProfiles = _.filter(matchingProfiles, (p) =>
          _.isObject(p.info),
        );
      }
      const result = _.map(matchingProfiles, (profile) => ({
        name: profile.name,
        url: profile.url,
        info: profile.info,
      }));
      return result;
    },
  },

  Mutation: {
    fetchProfileInfos(
      obj,
      { input: { filter = /.*/, securityToken = "" } = {} } = {},
    ) {
      verifySecurityToken(securityToken);
      const matchingProfiles = _.filter(profiles, ({ name }) =>
        filter.test(name),
      );
      return {
        results: _.map(matchingProfiles, async (profile) => {
          let success = false;
          if (!profile.fetchInfo) {
            return { profile };
          }
          try {
            await profile.fetchInfo();
            success = true;
          } catch (e) {
            // success remains as false
          }
          return {
            success,
            profile,
          };
        }),
      };
    },

    async verifyLinkedInLogin(
      obj,
      { input: { pin = "", securityToken = "" } = {} },
    ) {
      verifySecurityToken(securityToken);
      const expected = await acquirePin(pin);
      return { expected };
    },
  },
};
